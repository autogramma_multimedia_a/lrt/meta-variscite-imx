# Copyright (C) 2015 Freescale Semiconductor
# Copyright 2017-2021 NXP
# Copyright 2021 Autogramma
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Autogramma image for Linux Recovery Tool"
LICENSE = "MIT"

inherit core-image

IMAGE_FEATURES += " \
    debug-tweaks \
    splash \
    tools-debug \
    ssh-server-dropbear \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'weston', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'x11-base x11-sato', \
                                                       '', d), d)} \
"
CORE_IMAGE_EXTRA_INSTALL += " \
    packagegroup-core-full-cmdline \
    foot \
    autostart \
    simg2img \
    dhrystone \
    alsa-utils sof-tools \
    mpv \
"

# LICENSE_FLAGS_WHITELIST += " commercial "

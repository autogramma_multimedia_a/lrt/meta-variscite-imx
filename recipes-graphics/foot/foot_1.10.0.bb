# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3a7351a597a91e763901f7c76f21e798"

SRC_URI = "gitsm://codeberg.org/dnkl/foot.git;protocol=https"
SRCREV = "589404b32e908af038673e95b697c41e88cd62a0"

S = "${WORKDIR}/git"

DEPENDS = "tllist fcft wayland wayland-native wayland-protocols ncurses libxkbcommon"

inherit meson pkgconfig

FILES_${PN} += "${prefix}/share"

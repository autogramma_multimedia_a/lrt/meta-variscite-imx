# Copyright (C) 2021 Autogramma
LICENSE = "MIT & Unknown"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3a7351a597a91e763901f7c76f21e798 \
                    file://unicode/LICENSE;md5=69eb4720db1da91437cc896c8d3fcb41"

SRC_URI = "gitsm://codeberg.org/a1batross/fcft.git;protocol=https"
SRCREV = "eaf96e5a4148beccf707650df81f13571b562478"

S = "${WORKDIR}/git"
DEPENDS = "tllist pixman fontconfig freetype"

inherit meson pkgconfig

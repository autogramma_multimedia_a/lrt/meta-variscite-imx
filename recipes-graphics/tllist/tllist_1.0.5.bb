# Copyright (C) 2021 Autogramma
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3a7351a597a91e763901f7c76f21e798"

SRC_URI = "git://codeberg.org/dnkl/tllist.git;protocol=https"
SRCREV = "f2d806fe3e63a4f8b2da45883764c79696380e51"

S = "${WORKDIR}/git"

inherit meson pkgconfig

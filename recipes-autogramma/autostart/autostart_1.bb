# Copyright 2021 Autogramma

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

S = "${WORKDIR}"

SRC_URI = "file://autostart.service"

inherit systemd

do_install() {
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${S}/autostart.service        ${D}${systemd_unitdir}/system/

        install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
        ln -sf ${systemd_unitdir}/system/autostart.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/autostart.service
}

FILES_${PN} = "${sysconfdir}/systemd/system/multi-user.target.wants/ ${systemd_unitdir}/system"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/gpl-2.0.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"

DEPENDS += "bison-native"

UBOOT_SRC ?= "git://gitlab.com/autogramma_multimedia_a/vendor/variscite/uboot-imx;protocol=https"

SRCBRANCH = "lf_v2021.04_var01-lrt"
SRCREV = "${AUTOREV}"
UBOOT_INITIAL_ENV = "u-boot-initial-env"

SRCBRANCH_imx6ul-var-dart = "imx_v2018.03_4.14.78_1.0.0_ga_var02"
SRCREV_imx6ul-var-dart = "717f29898abe82ffa2d74515806c46094075285a"

SRC_URI = "${UBOOT_SRC};branch=${SRCBRANCH}"

S = "${WORKDIR}/git"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENCE;md5=f674ee93878e0b25c4e95dc6c2d06cdd \
                    file://src/audio/igo_nr/LICENSE;md5=5bfcc27cd1ad08d99d54170916a9620f \
                    file://rimage/LICENSE;md5=96d0c52cfb525713652d67584bda5632 \
                    file://rimage/tomlc99/LICENSE;md5=815f47e0a2e8e641301a9f6de604ccdc"

SRC_URI = "gitsm://gitlab.com/autogramma_multimedia_a/sof.git;protocol=https;branch=main"

PV = "1.9+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit cmake

DEPENDS = "alsa-lib"

OECMAKE_SOURCEPATH = "${S}/tools"
OECMAKE_TARGET_COMPILE = "sof-ctl sof-logger sof-probes"

do_install() {
	mkdir -p "${D}/usr/bin"
	install -m 755 "${B}/ctl/sof-ctl" "${D}/usr/bin/sof-ctl"
	install -m 755 "${B}/logger/sof-logger" "${D}/usr/bin/sof-logger"
	install -m 755 "${B}/probes/sof-probes" "${D}/usr/bin/sof-probes"
}

FILES_${PN} = "/usr/bin/sof-ctl /usr/bin/sof-logger /usr/bin/sof-probes"

# Helper function for do_configure to allow multiple configurations
# $1 the directory to run configure in
# $@ the arguments to pass to configure
ncurses_configure() {
	mkdir -p $1
	cd $1
	shift
	oe_runconf \
	        --without-debug \
	        --without-ada \
	        --without-gpm \
	        --enable-hard-tabs \
	        --enable-xmc-glitch \
	        --enable-colorfgbg \
	        --with-termpath='${sysconfdir}/termcap:${datadir}/misc/termcap${EX_TERMCAP}' \
	        --with-terminfo-dirs='${sysconfdir}/terminfo:${datadir}/terminfo${EX_TERMINFO}' \
	        --with-shared \
	        --program-prefix= \
	        --with-ticlib \
	        --with-termlib=${EX_TERMLIB} \
	        --enable-sigwinch \
	        --enable-pc-files \
	        --disable-rpath-hack \
		--enable-big-core \
		${EXCONFIG_ARGS} \
	        --with-manpage-format=normal \
	        --without-manpage-renames \
	        --disable-stripping \
	        "$@" || return 1
	cd ..
}
